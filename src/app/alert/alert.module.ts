import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlertPageComponent } from './alert-page/alert-page.component';
import { AlertTableComponent } from './alert-table/alert-table.component';
import { AlertDetailComponent } from './alert-detail/alert-detail.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';



@NgModule({
  declarations: [
    AlertPageComponent,
    AlertTableComponent,
    AlertDetailComponent
  ],
  imports: [
    CommonModule,
    NgxChartsModule
  ]
})
export class AlertModule { }

import { Component, Input } from '@angular/core';
import { LegendPosition } from '@swimlane/ngx-charts';
import { DateTime } from 'luxon';
import { Alert } from '../models/alert.model';

@Component({
  selector: 'app-alert-detail',
  templateUrl: './alert-detail.component.html',
  styleUrls: ['./alert-detail.component.scss']
})
export class AlertDetailComponent {

  // Options for the chart
  showXAxis = true;
  showXAxisLabel = true;
  xAxisLabel = 'DateTime';
  showYAxis = true;
  showYAxisLabel = true;
  yAxisLabel = 'Niveau [m]';
  gradient = false;
  showLegend = true;
  legendPosition = LegendPosition.Below;
  legendTitle = 'Légende'
  timeline = true;
  autoscale = false;
  yScaleMin = 21;
  yScaleMax = 25;

  // Color Scheme
  colorScheme = {
    domain: ['#9370DB', '#87CEFA', '#FA8072', '#FF7F50', '#90EE90', '#9370DB']
  };

  data: any[] = [
    {
      name: "Valeurs mesurées",
      series: []
    },
    {
      name: "Valeurs corrigées",
      series: []
    }
  ]

  /** Sensor ID */
  sensorID: string | null = null

  /** Alert Started At */
  alertStartedAt: string | null = null

  /** Alert Started At */
  alertEndedAt: string | null = null

  /**
   * 
   */
  @Input('alert') set setAlert(alert: Alert | null) {
    if (alert) {
      this.sensorID = alert.sensorID
      this.alertStartedAt = this.formatDateTime(alert.startedAt)
      this.alertEndedAt = this.formatDateTime(alert.endedAt)
      
      this.data = [
        {
          name: "Valeurs mesurées",
          series: alert.measurements.map(m => {
            return { name: this.formatDateTime(m.dateTime), value: m.measuredValue }
          })
        },
        {
          name: "Valeurs corrigées",
          series: alert.measurements.map(m => {
            return { name: this.formatDateTime(m.dateTime), value: m.predictedValue }
          })
        }
      ]
    }
  }

  /**
   * 
   * @param dt 
   * @returns 
   */
  formatDateTime(dt: string | undefined | null): string {
    if (!dt) return ''

    return DateTime
      .fromISO(dt, { zone: 'utc' })
      .toLocaleString(DateTime.DATETIME_SHORT)
  }

}

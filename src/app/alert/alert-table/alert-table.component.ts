import { Component, EventEmitter, Input, Output } from '@angular/core';
import { DateTime } from 'luxon';
import { Alert, ConfidenceLevel } from '../models/alert.model';

@Component({
  selector: 'app-alert-table',
  templateUrl: './alert-table.component.html',
  styleUrls: ['./alert-table.component.scss']
})
export class AlertTableComponent {

  /** Alert Event Emitter */
  @Output('selectAlert') alertEventEmitter = new EventEmitter<Alert>()

  /** Alert List */
  @Input('alertList') alertList: Alert[] = []

  /**
   * 
   * @param alert 
   */
  showDetails(alert: Alert) {
    this.alertEventEmitter.emit(alert)
  }
  
  /**
   * 
   * @param alert 
   */
  getConfidenceLevel(alert: Alert) {
    switch (alert.confidenceLevel) {
      case ConfidenceLevel.LOW:
        return 'Faible'
      case ConfidenceLevel.MEDIUM:
        return 'Moyen'
      case ConfidenceLevel.HIGH:
        return 'Elevé' 
      default:
        return ''
    }
  }

  /**
   * 
   * @param dt 
   * @returns 
   */
  formatDateTime(dt: string | null): string {
    if (!dt) return ''

    return DateTime
      .fromISO(dt, { zone: 'utc' })
      .toLocaleString(DateTime.DATETIME_SHORT)
  }

}

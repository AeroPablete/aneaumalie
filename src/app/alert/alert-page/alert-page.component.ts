import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Alert } from '../models/alert.model';
import { AlertService } from '../services/alert.service';

@Component({
  selector: 'app-alert-page',
  templateUrl: './alert-page.component.html',
  styleUrls: ['./alert-page.component.scss']
})
export class AlertPageComponent implements OnInit, OnDestroy {

  /** Data Subscription */
  dataSubscription: Subscription | null = null

  /** Alert List */
  alertList: Alert[] = []

  /** Selected Alert */
  selectedAlert: Alert | null = null

  /**
   * 
   * @param _alertService 
   */
  constructor(private _alertService: AlertService) { }

  /**
   * 
   */
  ngOnInit(): void {
    this.alertList = this._alertService.getAlertListSnapshot()

    this.dataSubscription = this._alertService.getAlertList()
      .subscribe(data => this.alertList = data)
  }

  /**
   * 
   */
  ngOnDestroy(): void {
    this.dataSubscription?.unsubscribe()
  }

  /**
   * 
   * @param alert 
   */
  onSelectAlert(alert: Alert) {
    this.selectedAlert = alert
  }

}

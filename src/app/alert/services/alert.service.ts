import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { SensorMeasurement } from 'src/app/simulation/models/sensor-measurement.model';
import { Alert } from '../models/alert.model';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  /** Alert List */
  private _alertList: Alert[] = []

  /** Alert List Subject */
  private _alertListSubject = new BehaviorSubject<Alert[]>([])

  /**
   * 
   */
  constructor() { }

  /**
   * 
   * @param alert 
   */
  startAlert(sensorID: string, startedAt: string, confidenceLevel: number) {
    const uuid = new Date().getTime().toString(16)

    const alert: Alert = {
      uuid: uuid,
      sensorID: sensorID,
      startedAt: startedAt,
      endedAt: null,
      confidenceLevel: confidenceLevel,
      measurements: []
    }

    this._alertList.unshift(alert)
    this._alertListSubject.next(this._alertList)
    return uuid
  }

  /**
   * 
   * @param alert 
   */
  updateAlert(uuid: string, measurement: SensorMeasurement) {
    const alert = this._alertList.find(elm => elm.uuid === uuid)
    alert!.measurements.push(measurement)
  }

  /**
   * 
   * @param alert 
   */
  endAlert(uuid: string, endedAt: string) {
    const alert = this._alertList.find(elm => elm.uuid === uuid)
    alert!.endedAt = endedAt
    this._alertListSubject.next(this._alertList)
  }

  /**
   * 
   */
  resetAlertList() {
    this._alertList = []
    this._alertListSubject.next(this._alertList)
  }

  /**
   * 
   * @returns 
   */
  getAlertListSnapshot() {
    return this._alertListSubject.getValue()
  }

  /**
   * 
   * @returns 
   */
  getAlertList() {
    return this._alertListSubject.asObservable()
  }
}

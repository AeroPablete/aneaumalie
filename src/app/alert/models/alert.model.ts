import { SensorMeasurement } from "src/app/simulation/models/sensor-measurement.model"

export enum ConfidenceLevel {
    LOW     = 0,
    MEDIUM  = 1,
    HIGH    = 2
}

export interface Alert {
    uuid: string | null,
    sensorID: string
    startedAt: string
    endedAt: string | null,
    confidenceLevel: ConfidenceLevel, 
    measurements: SensorMeasurement[]
}
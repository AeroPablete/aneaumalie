import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AlertPageComponent } from "./alert/alert-page/alert-page.component";
import { SimulationPageComponent } from "./simulation/simulation-page/simulation-page.component";

const routes: Routes = [
    { path: 'simulation', component: SimulationPageComponent },
    { path: 'alerts', component: AlertPageComponent },
    { path: '**', redirectTo: '/simulation' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
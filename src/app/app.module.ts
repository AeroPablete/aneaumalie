import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouteReuseStrategy } from '@angular/router';
import { AlertModule } from './alert/alert.module';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { CustomRouteReuseStrategy } from './CustomRouteReuseStrategy';
import { SharedModule } from './shared/shared.module';
import { SimulationModule } from './simulation/simulation.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AlertModule,
    SharedModule,
    SimulationModule
  ],
  providers: [
    {
      provide: RouteReuseStrategy,
      useClass: CustomRouteReuseStrategy,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

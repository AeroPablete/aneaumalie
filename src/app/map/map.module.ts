import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { MapPageComponent } from './map-page/map-page.component';
import { MapComponent } from './map/map.component';



@NgModule({
  declarations: [
    MapPageComponent,
    MapComponent
  ],
  imports: [
    CommonModule,
    LeafletModule
  ],
  exports: [
    MapPageComponent
  ]
})
export class MapModule { }

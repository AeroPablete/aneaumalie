import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { circle, icon, latLng, marker, Marker, tileLayer } from 'leaflet';
import { Subscription } from 'rxjs';
import { Alert, ConfidenceLevel } from 'src/app/alert/models/alert.model';
import { AlertService } from 'src/app/alert/services/alert.service';
import { Sensor } from 'src/app/data/models/sensor.model';
import { DataService } from 'src/app/data/services/data.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit, OnDestroy  {

  /** Map Options */
  mapOptions: any

  /** Map Layers */
  mapLayers: any

  /** Alert Subscription */
  alertSubscription: Subscription | null = null

  /**
   * 
   * @param _dataService 
   */
  constructor(
    private _router: Router,
    private _alertService: AlertService,
    private _dataService: DataService) {}
  
  /**
   * 
   */
  ngOnInit(): void {
    this._initMap()
    this._loadMapLayers([])

    // Alert Subscription
    this._alertService.getAlertList()
      .subscribe(alertList => this._loadMapLayers(alertList))
  }

  /**
   * 
   */
  ngOnDestroy(): void {
    this.alertSubscription?.unsubscribe()
  }

  /**
   * 
   */
  private _initMap() {
    // Get Sensor Data
    const sensorList = this._dataService.sensorList

    // Compute Map Center
    const latitude = sensorList.reduce((sum, sensor) => sum + sensor.geolocation.lat, 0) / sensorList.length
    const longitude = sensorList.reduce((sum, sensor) => sum + sensor.geolocation.lng, 0) / sensorList.length

    // Map Config
    this.mapOptions = {
      layers: [tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: '...' })],
      zoom: 14,
      center: latLng(latitude, longitude)
    };
  }

  /**
   * 
   */
  private _loadMapLayers(alertList: Alert[]) {
    // Get Sensor Data
    const sensorList = this._dataService.sensorList    

    // Add Layers to Map
    const markers = this._loadMarkers(alertList, sensorList)
    const alerts = this._loadAlerts(alertList, sensorList)
    this.mapLayers= [...markers, ...alerts]
  }

  /**
   * 
   * @param sensorList 
   * @returns 
   */
  private _loadMarkers(alertList: Alert[], sensorList: Sensor[]) {
    const sensorsWithAlert = alertList
      .filter(item => !item.endedAt)
      .map(item => item.sensorID)

    return sensorList
      .filter(item => !sensorsWithAlert.includes(item.id))
      .map(sensor => {
        const mapMarker = marker(
          [ sensor.geolocation.lat, sensor.geolocation.lng ],
          {
            title: sensor.id,
            icon: icon({
              iconUrl: "assets/sensor-marker.png",
              iconSize: [48, 48]
            })
          })

        return mapMarker
    })
  }

  /**
   * 
   * @param alertList 
   * @param sensorList 
   */
  private _loadAlerts(alertList: Alert[], sensorList: Sensor[]) {
    const sensorsWithAlert = alertList
      .filter(item => !item.endedAt)
      .map(item => item.sensorID)

    return sensorList
      .filter(item => sensorsWithAlert.includes(item.id))
      .map(sensor => {
        const indiceConfiance = alertList
          .find(item => !item.endedAt && item.sensorID === sensor.id)!.confidenceLevel
        
        let iconUrl
        switch (indiceConfiance) {
          case ConfidenceLevel.LOW:
            iconUrl = 'assets/alert-low-marker.png'
            break;
          case ConfidenceLevel.MEDIUM:
            iconUrl = 'assets/alert-medium-marker.png'
            break;
          case ConfidenceLevel.HIGH:
            iconUrl = 'assets/alert-high-marker.png'
            break;
          default:
            iconUrl = ''
            break;
        }

        const mapMarker = marker(
          [ sensor.geolocation.lat, sensor.geolocation.lng ],
          {
            title: sensor.id,
            icon: icon({
              iconUrl: iconUrl,
              iconSize: [48, 48]
            })
          })
        
        // Bind Click Handler  
        mapMarker.on('click', () => {
          this._router.navigate(['/alerts'])
        })  
        return mapMarker
    })
  }

}

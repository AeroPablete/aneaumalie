import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { firstValueFrom, of, switchMap } from 'rxjs';
import { RawData } from '../models/raw-data.model';
import { Sensor } from '../models/sensor.model';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  /** Raw Data */
  private _data: RawData[] = []

  /** Sensor List */
  private _sensorList: Sensor[] = []
  
  /**
   * 
   * @param _httpClient 
   */
  constructor(private _httpClient: HttpClient) {
    this._generateSensorList()
  }

  /**
   * Generate Sensor List
   */
   private _generateSensorList() {
    this._sensorList = [
      {
        id: "Aval",
        geolocation: { lng: -73.9008684, lat: 45.3152682 }
      },
      {
        id: "Quai",
        geolocation: { lng: -73.8786597, lat: 45.3167218 }
      }
    ]
  }

  /**
   * 
   */
  loadData() {
    return this._httpClient
      .get<RawData[]>('/assets/data.json')
      .pipe(
        switchMap((data: RawData[]) => {
          this._data = data
          return of(data)
        })
      )
  }

  /**
   * 
   */
  get rawData() {
    return this._data
  }

  /**
   * 
   */
  get sensorList() {
    return this._sensorList
  }
}

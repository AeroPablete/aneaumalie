export interface Sensor {
    id: string,
    geolocation: { lng: number, lat: number }
}
export interface RawData {
    'dateTime':  string,
    'sensor': { 
        'sensorID': string, 
        'measured_values': number, 
        'corrected_values': number,
        'error': number,
        'confidence_level': number
    }[]
}
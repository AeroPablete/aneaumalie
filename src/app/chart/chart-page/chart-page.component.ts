
import { Component, OnInit } from '@angular/core';
import { Sensor } from 'src/app/data/models/sensor.model';
import { DataService } from 'src/app/data/services/data.service';

@Component({
  selector: 'app-chart-page',
  templateUrl: './chart-page.component.html',
  styleUrls: ['./chart-page.component.scss']
})
export class ChartPageComponent implements OnInit {

  /** Sensor List */
  sensorList: Sensor[] = []

  /** Selected Sensor ID */
  selectedSensorID = "";

  /**
   * 
   * @param _simulationService 
   */
  constructor(
    private _dataService: DataService) { }

  /**
   * 
   */
  ngOnInit(): void {
    this.sensorList = this._dataService.sensorList
    this.selectedSensorID = this.sensorList[0].id
  }

  /**
   * 
   * @param sensorID 
   */
  selectTab(sensorID: string) {
    this.selectedSensorID = sensorID
  }
}

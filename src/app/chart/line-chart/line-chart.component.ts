import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { LegendPosition } from '@swimlane/ngx-charts';
import { DateTime } from 'luxon';
import { Subscription } from 'rxjs';
import { Alert } from 'src/app/alert/models/alert.model';
import { AlertService } from 'src/app/alert/services/alert.service';
import { SensorMeasurement } from 'src/app/simulation/models/sensor-measurement.model';
import { SimulationService } from 'src/app/simulation/services/simulation.service';

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.scss']
})
export class LineChartComponent implements OnInit, OnDestroy {

  // Options for the chart
  showXAxis = true;
  showXAxisLabel = true;
  xAxisLabel = 'DateTime';
  showYAxis = true;
  showYAxisLabel = true;
  yAxisLabel = 'Niveau [m]';
  gradient = false;
  showLegend = true;
  legendPosition = LegendPosition.Below;
  legendTitle = 'Légende'
  timeline = true;
  autoscale = true;
  yScaleMin = 21;
  yScaleMax = 25;

  // Color Scheme
  colorScheme = {
    domain: ['#9370DB', '#87CEFA', '#FA8072', '#FF7F50', '#90EE90', '#9370DB']
  };

  data: any[] = [
    {
      name: "Valeurs mesurées",
      series: []
    },
    {
      name: "Valeurs corrigées",
      series: []
    }
  ]

  /** Sensor Measurement Subscription */
  sensorMeasurementSubscription: Subscription | null = null

  /** Is Sim Running Subscription */
  isSimRunningSubscription: Subscription | null = null

  /** Sensor ID */
  @Input('sensorID') sensorID: string = ''

  /** Sensor Measurements */
  measurements: SensorMeasurement[] = []
  measurementErrorAlert: string | null = null

  /**
   * 
   * @param _simulationService 
   */
  constructor(
    private _alertService: AlertService,
    private _simulationService: SimulationService) {}

  /**
   * 
   */  
  ngOnInit(): void {
    this.isSimRunningSubscription = this._simulationService.isSimRunning()
      .subscribe(value => {
        if (!value) {
          this.measurements = []
          this._generateTimeSeries()
          this._alertService.resetAlertList()
        }
      })

    this.sensorMeasurementSubscription = this._simulationService.sensorMeasurementSubject
      .subscribe(lookup => {
        if (lookup) {
          const newMeasurement = lookup[this.sensorID]          

          // Check anomalies
          this._checkAnomalies(newMeasurement)          

          // Generation des Time Series
          this.measurements = [...this.measurements, newMeasurement]
          if (this.measurements.length > 290) this.measurements.shift()          
          this._generateTimeSeries()
        }
      })
  }

  /** */
  ngOnDestroy(): void {
    this.sensorMeasurementSubscription?.unsubscribe()
  }

  /**
   * 
   */
  private _checkAnomalies(measurement: SensorMeasurement) {
    if (measurement.sensorError) {
      if (!this.measurementErrorAlert) {
        this.measurementErrorAlert = this._alertService
          .startAlert(this.sensorID, measurement.dateTime!, measurement.confidenceLevel!)
        this._alertService.updateAlert(this.measurementErrorAlert, measurement)
      }
      else {
        this._alertService.updateAlert(this.measurementErrorAlert, measurement)
      }            
    }

    if (!measurement.sensorError && this.measurementErrorAlert) {
      this._alertService.updateAlert(this.measurementErrorAlert, measurement)
      this._alertService.endAlert(this.measurementErrorAlert, measurement.dateTime!)
      this.measurementErrorAlert = null
    }
  }

  /**
   * 
   */
  private _generateTimeSeries() {
    this.data = [
      {
        name: "Valeurs mesurées",
        series: this.measurements.map(m => {
          return { name: this._formatDateTime(m.dateTime), value: m.measuredValue }
        })
      },
      {
        name: "Valeurs corrigées",
        series: this.measurements.map(m => {
          return { name: this._formatDateTime(m.dateTime), value: m.predictedValue }
        })
      }
    ]
  }

  /**
   * 
   * @param dt 
   * @returns 
   */
  private _formatDateTime(dt: string | undefined): string {
    if (!dt) return ''

    return DateTime
      .fromISO(dt, { zone: 'utc' })
      .toLocaleString(DateTime.DATETIME_SHORT)
  }
}

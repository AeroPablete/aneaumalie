import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxChartsModule } from '@swimlane/ngx-charts';

import { ChartPageComponent } from './chart-page/chart-page.component';
import { LineChartComponent } from './line-chart/line-chart.component';


@NgModule({
  declarations: [
    ChartPageComponent,
    LineChartComponent
  ],
  imports: [
    CommonModule,
    NgxChartsModule
  ],
  exports: [
    ChartPageComponent
  ]
})
export class ChartModule { }

import { ActivatedRouteSnapshot, DetachedRouteHandle, RouteReuseStrategy } from '@angular/router'

export class CustomRouteReuseStrategy implements RouteReuseStrategy {

    /** Stored Routes */
    private storedRoutes = new Map<string, DetachedRouteHandle>();

    shouldDetach(route: ActivatedRouteSnapshot): boolean {
        return !!route.routeConfig && ['alerts', 'simulation'].includes(route.routeConfig.path as string);
    }

    store(route: ActivatedRouteSnapshot, handle: DetachedRouteHandle | null): void {
        const key = route.routeConfig?.path || ''
        
        if (!!key && !!handle) {
            this.storedRoutes.set(key, handle); 
        }       
    }

    shouldAttach(route: ActivatedRouteSnapshot): boolean {
        return (!!route.routeConfig && !!this.storedRoutes.get(route.routeConfig.path as string));
    }

    retrieve(route: ActivatedRouteSnapshot): DetachedRouteHandle | null {
        const key = route.routeConfig?.path || ''
        return this.storedRoutes.get(key) || null;
    }

    shouldReuseRoute(future: ActivatedRouteSnapshot, curr: ActivatedRouteSnapshot): boolean {
        return future.routeConfig === curr.routeConfig;
    }
}
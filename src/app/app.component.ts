import { Component, OnDestroy, OnInit } from '@angular/core';
import { AlertService } from './alert/services/alert.service';

import { Subscription } from 'rxjs'

import { SimulationService } from './simulation/services/simulation.service';
import { DataService } from './data/services/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  /** Alert Count */
  alertCount: number = 0

  /** Is Loading Data Flag */
  isLoadingData: boolean = true;

  /** Is Simulation Running Flag */
  isSimRunning: boolean = false;

  /** Data Subscription */
  dataSubscription: Subscription | null = null

  /** Alert Count Subscription */
  alertCountSubscription: Subscription | null = null

  /** Sim Running Subscription */
  simRunningSubscription: Subscription | null = null

  /**
   * 
   * @param _dataService 
   */
  constructor(
    private _alertService: AlertService,
    private _dataService: DataService,
    private _simulationService: SimulationService) { }

  /**
   * 
   */
  ngOnInit(): void {
    this.dataSubscription = this._dataService.loadData()
      .subscribe((_) => {
        setTimeout(() => { this.isLoadingData = false }, 2000)
      })

    this.simRunningSubscription = this._simulationService.isSimRunning()
      .subscribe((value) => this.isSimRunning = value)

    this.alertCountSubscription = this._alertService.getAlertList()
      .subscribe(data => this.alertCount = data.length)
  }

  /**
   * 
   */
  ngOnDestroy(): void {
    this.dataSubscription?.unsubscribe();
    this.simRunningSubscription?.unsubscribe();
    this.alertCountSubscription?.unsubscribe();
  }

  /**
   * 
   */
  startSimulation() {
    this._simulationService.startSimulation()
  }
}

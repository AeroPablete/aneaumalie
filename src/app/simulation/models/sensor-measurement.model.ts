export interface SensorMeasurement {
    sensorID: string
    dateTime?: string
    measuredValue: number
    predictedValue: number
    sensorError: boolean
    confidenceLevel: number | null
}
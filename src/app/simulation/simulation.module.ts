import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SimulationPageComponent } from './simulation-page/simulation-page.component';
import { ChartModule } from '../chart/chart.module';
import { DataModule } from '../data/data.module';
import { MapModule } from '../map/map.module';



@NgModule({
  declarations: [
    SimulationPageComponent
  ],
  imports: [
    CommonModule,
    ChartModule,
    DataModule,
    MapModule
  ]
})
export class SimulationModule { }

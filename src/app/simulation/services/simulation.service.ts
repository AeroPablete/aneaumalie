import { Injectable } from '@angular/core';
import { BehaviorSubject, interval, map, take } from 'rxjs';
import { RawData } from '../../data/models/raw-data.model';
import { SensorMeasurement } from '../models/sensor-measurement.model';
import { DataService } from '../../data/services/data.service';

@Injectable({
  providedIn: 'root'
})
export class SimulationService {

  /** Is Running Subject */
  private _isSimRunningSubject = new BehaviorSubject<boolean>(false)

  /** Sensor Measurement Subject */
  private _sensorMeasurementSubject = new BehaviorSubject<{[key: string]: SensorMeasurement} | null>(null)

  /**
   * 
   * @param _dataService 
   */
  constructor(private _dataService: DataService) { }

  /**
   * 
   */
  async startSimulation() {
    this._isSimRunningSubject.next(true)
    const rawData = this._dataService.rawData

    const source = interval(1000)
      .pipe(
        take(rawData.length),
        map(index => { 
          const lookup = this._mapRow(rawData[index])
          this._sensorMeasurementSubject.next(lookup)

          // Vérifier s'il s'agit de la dernière valeur émise
          const isLastValue = index === rawData.length - 1
          if (isLastValue) {
            setTimeout(() => this._isSimRunningSubject.next(false), 1000)
          }
        })
      )

    source.subscribe()
  }

  /**
   * 
   */
  private _mapRow(row: RawData) {
    const dateTime = row['dateTime'].split(' ').join('T').replace('Z', '.000Z')
    const sensorList = row['sensor']

    return sensorList
      .map(s => ({ 
        dateTime: dateTime, 
        sensorID: s['sensorID'], 
        measuredValue: s['measured_values'], 
        predictedValue: s['corrected_values'], 
        sensorError: s['error'] === 1,
        confidenceLevel: s['confidence_level'] >= 0 ? s['confidence_level'] : null }) as SensorMeasurement)
      .reduce(
        (d, m) => {
          d[m.sensorID] = m
          return d
        },
        {} as { [key: string]: SensorMeasurement })
  }

  /**
   * 
   * @returns 
   */
  isSimRunning() {
    return this._isSimRunningSubject.asObservable()
  }

  /**
   * 
   */
  get sensorMeasurementSubject() {
    return this._sensorMeasurementSubject.asObservable()
  }
}
